﻿using UnityEngine;
using System.Collections;

public class SinglePlayerScene : MonoBehaviour 
{
	public GameObject redTeamSpawnPoint;
	public GameObject blueTeamSpawnPoint;

	private int spawnDelay;
	private bool isSpawnTime;

	// Use this for initialization
	void Start () 
	{
		spawnDelay = 0;
		isSpawnTime = false;
		initScene ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		UpdateCreepsSpawn();
	}

	private void UpdateCreepsSpawn()
	{
		if (GameManager.GameClock != 0)
		{
			Debug.Log("Spawner " + GameManager.GameClock);

			if (GameManager.GameClock % 5 == 0) 
			{
				isSpawnTime = true;
			}

			if (isSpawnTime)
			{			
				if (spawnDelay == 0)
					SpawnMercenaries ();
			
				if (spawnDelay == 30)
					SpawnMercenaries ();
			
				if (spawnDelay == 60)
					SpawnMercenaries ();
			
				if (spawnDelay == 90)
					SpawnHunters ();
			
				if (spawnDelay == 120)
					SpawnHunters ();
			
				if (spawnDelay == 150)
				{
					SpawnDiciples ();
					isSpawnTime = false;
					spawnDelay = 0;
				}

				spawnDelay++;
			}
		}
	}

	private void initScene()
	{
		//GameManager.CreateCreepAt("Debugger", ClassType.MERCENARY, TeamColor.RED_TEAM, false, redTeamSpawnPoint.transform.position);

		GameManager.CreateTowerAt(TeamColor.RED_TEAM, new Vector3(40, 0, 0));
		GameManager.CreateTowerAt(TeamColor.BLUE_TEAM, new Vector3(-40, 0, 0));
		
		GameManager.CreateCastleAt(TeamColor.RED_TEAM, new Vector3(100, 0, 0));
		GameManager.CreateCastleAt(TeamColor.BLUE_TEAM, new Vector3(-100, 0, 0));
	}

	private void SpawnMercenaries()
	{
		GameManager.CreateCreepAt("Red_Mercenary", ClassType.MERCENARY, TeamColor.RED_TEAM, false, redTeamSpawnPoint.transform.position);
		GameManager.CreateCreepAt("Blue_Mercenary", ClassType.MERCENARY, TeamColor.BLUE_TEAM, false, blueTeamSpawnPoint.transform.position);
	}

	private void SpawnHunters()
	{
		//GameManager.CreateCreepAt("Red_Hunter", ClassType.HUNTER, TeamColor.RED_TEAM, false, redTeamSpawnPoint.transform.position);
		//GameManager.CreateCreepAt("Blue_Hunter", ClassType.HUNTER, TeamColor.BLUE_TEAM, false, blueTeamSpawnPoint.transform.position);
	}

	private void SpawnDiciples()
	{
		//GameManager.CreateCreepAt("Red_Diciple", ClassType.DISCIPLE, TeamColor.RED_TEAM, false, redTeamSpawnPoint.transform.position);
		//GameManager.CreateCreepAt("Blue_Diciple", ClassType.DISCIPLE, TeamColor.BLUE_TEAM, false, blueTeamSpawnPoint.transform.position);
	}
}
