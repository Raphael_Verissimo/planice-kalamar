﻿using UnityEngine;
using System.Collections;

public class TargetAnimation : MonoBehaviour
{
	public int duration;
	public int count;

	// Use this for initialization
	void Start () 
	{
		count = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{ 
		count++;

		if (count >= duration)
			Destroy(this.gameObject);	

		transform.localScale = transform.localScale * 0.9f;
	}
}
