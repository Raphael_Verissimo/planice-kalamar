using UnityEngine;
using System.Collections;

/**
 * Classe que Controla as Barras de Vida e Suas Anima�oes.
 */ 
public class LifeBar : MonoBehaviour
{
	private GameObject greenBar;
	private GameObject redBar;
	private TextMesh lifeCounter;

	private float targetAnimationScale;	
	private float targetAnimationScaleSpeed;

	private bool hasDamageAnimation;

	private int currentLife;
	private int maxLife;
	
	private Vector3 fullLifeScale;
	private Vector3 fullLifePosition;
	private Quaternion origRotationPosition;

	public int CurrentLife { get{return currentLife;} }
	public int MaxLife { get{return maxLife;} }

	private bool isConfigured;

	public GameObject GreenBar 
	{
		get
		{
			if(greenBar == null)
				greenBar = transform.FindChild("GreenBar").gameObject;

			return greenBar;
		}
	}

	public GameObject RedBar 
	{
		get
		{
			if(redBar == null)
				redBar = transform.FindChild("RedBar").gameObject;
			
			return redBar;
		}
	}

	public TextMesh LifeCounter 
	{
		get
		{
			if(lifeCounter == null)
				lifeCounter = transform.FindChild("LifeCounter").gameObject.GetComponent<TextMesh>();
			
			return lifeCounter;
		}
	}

	void Start()
	{
		initBar();
	}

	void Update()
	{
		if(hasDamageAnimation)
			UpdateLifeBarAnimation ();

		UpdateLookAt ();
	}

	private void initBar()
	{
		if (!isConfigured) 
		{
			isConfigured = true;
			fullLifePosition = GreenBar.transform.localPosition;
			transform.up = Vector3.up;
			origRotationPosition = transform.rotation;
			
			//maxLife = 0;
			currentLife = maxLife;
			LifeCounter.text = maxLife.ToString ();
			
			fullLifeScale = GreenBar.transform.localScale;
			targetAnimationScaleSpeed = 0.01f;
		}
	}

	/**
	 * Utilize para Incrementar a Vida dos Personagens, por padrao ao Aumentar a Vida, ela eh Curada.
	 */
	public void SetUpLifeBar(CharacterController character)
	{
		this.currentLife = character.CharacterModel.MaxHitPoints;
		this.maxLife = character.CharacterModel.MaxHitPoints;

		initBar();
		SetFullLife ();
	}

	/**
	 * Aplica Diferen�a de Danos a Barra, Dano = Negativo, Cura = Positivo;
	 */
	public void UpdateBar(int amount)
	{
		SetDamageAnimation(amount);
	}

	/**
	 * Utilizada internamente para Aplicar Dano e Configurar a Anima�ao da Barra Vermelha.
	 */
	private void SetDamageAnimation(int amount)
	{
		hasDamageAnimation = true;
		int receivedDamage = amount;

		if (amount < 0)
		{
			receivedDamage = -amount;

			if(receivedDamage > currentLife)
				receivedDamage = currentLife;
		}

		float damagePercent = receivedDamage / (float)maxLife;				
		float lifeScale = fullLifeScale.x * damagePercent;
		targetAnimationScale = lifeScale;
		
		Vector3 newScale = new Vector3(lifeScale, 0, 0);
		Vector3 newPosition = new Vector3(lifeScale/2, 0, 0);

		currentLife += amount;

		if (currentLife > maxLife)
			currentLife = maxLife;

		if (currentLife < 0)
			currentLife = 0;

		lifeCounter.text = currentLife.ToString();

		if(amount < 0)
		{						
			GreenBar.transform.localScale -= newScale;
			GreenBar.transform.localPosition += newPosition;			
		}
		else
		{
			GreenBar.transform.localScale += newScale;
			GreenBar.transform.localPosition -= newPosition;

			if (RedBar.transform.localScale.x < GreenBar.transform.localScale.x)
			{
				RedBar.transform.localScale = GreenBar.transform.localScale;
				RedBar.transform.localPosition = GreenBar.transform.localPosition;

				hasDamageAnimation = false;
			}
		}
		
		if(currentLife >= maxLife)
			SetFullLife();
	}

	/**
	 * Ajusta a Barra para Vida Cheia.
	 */
	private void SetFullLife()
	{
		GreenBar.transform.localScale = fullLifeScale;
		GreenBar.transform.localPosition = new Vector3(fullLifePosition.x, fullLifePosition.y, 0.15f); 

		RedBar.transform.localScale = fullLifeScale;
		RedBar.transform.localPosition = new Vector3(fullLifePosition.x, fullLifePosition.y, 0.1f); 

		LifeCounter.text = maxLife.ToString ();
	}

	/**
	 * Controla a Movimenta�ao da Barra Vermelha.
	 */
	private void UpdateLifeBarAnimation()
	{
		if (RedBar.transform.localScale.x > GreenBar.transform.localScale.x)
		{
			Vector3 newScale = new Vector3(targetAnimationScaleSpeed, 0, 0);
			Vector3 newPosition = new Vector3(targetAnimationScaleSpeed/2, 0, 0);

			RedBar.transform.localScale -= newScale;
			RedBar.transform.localPosition += newPosition;		
		}
		else
		{
			hasDamageAnimation = false;
		}
	}

	/**
	 * Mantem as barras de vida viradas para a camera
	 */
	private void UpdateLookAt()
	{
		transform.LookAt (GameManager.MainCamera.transform);
	}
}