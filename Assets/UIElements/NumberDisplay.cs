using UnityEngine;
using System.Collections;

/**
 * Classe que exibe a numera��o colorida, utilizada pela DisplayController.
 */ 
public class NumberDisplay : MonoBehaviour
{
	private TextMesh displayText;

	private string currentText;
	private Color currentColor;
	private float scaleFactor;
	private CharacterController character;
	private Vector3 characterPosition;

	private bool shownRequested;
	private bool isHealing;

	private float animationSpeed;
	private float animationGravity;

	void Start()
	{
		animationSpeed = 0.2f;
		animationGravity = 0.005f;
	}

	void Update ()
	{
		UpdateAnimation ();
	}

	public void ShowDamage(CharacterController character, int amount, Color color)
	{			
		this.character = character;

		amount = amount * -1;

		characterPosition = character.CharacterPosition;	
		transform.position = characterPosition;
		transform.position += Vector3.up * 5;
		transform.position -= Vector3.left * 0.5f;

		displayText = transform.GetComponentInChildren<TextMesh>();
		displayText.text = amount.ToString();
		displayText.color = color;

		scaleFactor = 0.8f + ((float)amount / 1000);
		
		if (scaleFactor > 2)
			scaleFactor = 2;
		
		this.transform.localScale *= scaleFactor;

		shownRequested = true;
	}
		
	public void ShowHealing(CharacterController character, int amount, Color color)
	{			
		this.character = character;

		characterPosition = character.CharacterPosition;	
		transform.position = characterPosition;
		transform.position += Vector3.up * 5;
		transform.position -= Vector3.left * 0.5f;

		displayText = transform.GetComponentInChildren<TextMesh>();
		displayText.text = "+"+amount.ToString();
		displayText.color = color;
		
		scaleFactor = 0.8f + ((float)amount / 1000);
		
		if (scaleFactor > 2)
			scaleFactor = 2;
		
		this.transform.localScale *= scaleFactor;
		
		shownRequested = true;
		isHealing = true;
	}

	private void UpdateAnimation()
	{
		if (shownRequested)
		{
			if (isHealing)
			{
				transform.position += new Vector3 (0, animationSpeed/2.5f, 0);
				
				if (transform.position.y > 10)
					DestroyObject (this.gameObject);
			}
			else
			{
				animationSpeed -= animationGravity;
				
				transform.position += new Vector3 (0.02f , animationSpeed * 0.75f, 0);
				this.transform.localScale *= 0.995f;
				
				if (transform.position.y < 0)
					DestroyObject (this.gameObject);
			}

			transform.LookAt (GameManager.MainCamera.transform);
		}
	}
}