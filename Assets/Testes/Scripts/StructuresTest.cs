﻿using UnityEngine;
using System.Collections;

public class StructuresTest : MonoBehaviour 
{
	public CharacterController ally1;
	public CharacterController ally2;
	public CharacterController ally3;
	public CharacterController ally4;
	public CharacterController ally5;
	public CharacterController ally6;

	public CharacterController tower;
	public CharacterController castle;

	public CharacterController player;

	public int testTimer;

	// Use this for initialization
	void Start ()
	{
		testTimer = 0;
		//ReSpawn ();

		//SpanwPlayer ();
	}

	// Update is called once per frame
	void Update () 
	{
		testTimer++;

		if(testTimer == 1)
			SpawnTower ();

		if(testTimer == 1)
			SpawnCastle ();
		
		if(testTimer == 10)
			SpawnMinions ();
	}

	public void SpawnMinions()
	{
		if(ally1!=null) Destroy(ally1.CharacterGO);
		if(ally2!=null) Destroy(ally2.CharacterGO);
		if(ally3!=null) Destroy(ally3.CharacterGO);
		if(ally4!=null) Destroy(ally4.CharacterGO);
		if(ally5!=null) Destroy(ally5.CharacterGO);
		if(ally6!=null) Destroy(ally6.CharacterGO);

		Vector3 spawn1 = new Vector3 (0, 1, 0);
		Vector3 spawn2 = new Vector3 (0, 1, -5);
		Vector3 spawn3 = new Vector3 (0, 1, -10);
		Vector3 spawn4 = new Vector3 (-3, 1, -3);
		Vector3 spawn5 = new Vector3 (-3, 1, -8);
		Vector3 spawn6 = new Vector3 (-5, 1, -5);		

		ally1 = GameManager.CreateCharacterAt("Ally1", ClassType.MERCENARY, TeamColor.RED_TEAM, false, spawn1);
		ally2 = GameManager.CreateCharacterAt("Ally2", ClassType.MERCENARY, TeamColor.RED_TEAM, false, spawn2);
		ally3 = GameManager.CreateCharacterAt("Ally3", ClassType.MERCENARY, TeamColor.RED_TEAM, false, spawn3);
		ally4 = GameManager.CreateCharacterAt("Ally4", ClassType.HUNTER, TeamColor.RED_TEAM, false, spawn4);
		ally5 = GameManager.CreateCharacterAt("Ally5", ClassType.HUNTER, TeamColor.RED_TEAM, false, spawn5);
		ally6 = GameManager.CreateCharacterAt("Ally6", ClassType.DISCIPLE, TeamColor.RED_TEAM, false, spawn6);
	
		ally1.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		ally2.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		ally3.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		ally4.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		ally5.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		ally6.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);

		ally1.UseAction (ActionType.CLICK_TARGET, new Vector3 (-35, 1, 0));
		ally2.UseAction (ActionType.CLICK_TARGET, new Vector3 (-35, 1, 0));
		ally3.UseAction (ActionType.CLICK_TARGET, new Vector3 (-35, 1, 0));
		ally4.UseAction (ActionType.CLICK_TARGET, new Vector3 (-35, 1, 0));
		ally5.UseAction (ActionType.CLICK_TARGET, new Vector3 (-35, 1, 0));
		ally6.UseAction (ActionType.CLICK_TARGET, new Vector3 (-35, 1, 0));
	}

	public void SpawnTower()
	{
		if(tower!=null) Destroy(tower.CharacterGO);

		Vector3 spawn = new Vector3 (-20, 1, 0);

		tower = GameManager.CreateTowerAt(TeamColor.BLUE_TEAM, spawn);
	}

	public void SpawnCastle()
	{
		if(castle!=null) Destroy(castle.CharacterGO);
		
		Vector3 spawn = new Vector3 (-55, 1, 0);
		
		castle = GameManager.CreateCastleAt(TeamColor.BLUE_TEAM, spawn);
	}
	 
	public void SpanwMage()
	{
		SpanwPlayer (ClassType.MAGE);
	}

	public void SpanwArcher()
	{
		SpanwPlayer (ClassType.ARCHER);
	}

	public void SpanwWarrior()
	{
		SpanwPlayer (ClassType.WARRIOR);
	}

	public void SpanwPlayer(ClassType playerClass)
	{
		if (player != null)
			Destroy (player.CharacterGO);

		player = GameManager.CreateCharacter ("Player", playerClass, TeamColor.RED_TEAM, true);
	}
}
