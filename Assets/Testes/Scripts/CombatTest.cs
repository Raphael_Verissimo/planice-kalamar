﻿using UnityEngine;
using System.Collections;

public class CombatTest : MonoBehaviour 
{
	public CharacterController enemy1;
	public CharacterController enemy2;
	public CharacterController enemy3;
	public CharacterController enemy4;
	public CharacterController enemy5;
	public CharacterController enemy6;

	public CharacterController ally1;
	public CharacterController ally2;
	public CharacterController ally3;
	public CharacterController ally4;
	public CharacterController ally5;
	public CharacterController ally6;

	public CharacterController player;

	public int spawnCount;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		spawnCount++;

		if(spawnCount == 1)
		{
			//DebugCode();
			SpawnMercenaries();
		}

		if(spawnCount == 200)
		{
			SpawnHunters();
		}

		if(spawnCount == 350)
		{
			SpawnDiciples();
		}

		if((spawnCount % 1500) == 0)
		{
			//RefreshTargets();
		}
	}

	public void SpawnMercenaries()
	{
		if(enemy1!=null) Destroy(enemy1.CharacterGO);
		if(enemy2!=null) Destroy(enemy2.CharacterGO);
		if(enemy3!=null) Destroy(enemy3.CharacterGO);
		if(ally1!=null) Destroy(ally1.CharacterGO);
		if(ally2!=null) Destroy(ally2.CharacterGO);
		if(ally3!=null) Destroy(ally3.CharacterGO);

		Vector3 spawn1 = new Vector3 (-10, 1, 0);
		Vector3 spawn2 = new Vector3 (-10, 1, -5);
		Vector3 spawn3 = new Vector3 (-10, 1, -10);
		Vector3 spawn7 = new Vector3 (10, 1, 0);
		Vector3 spawn8 = new Vector3 (10, 1, -5);
		Vector3 spawn9 = new Vector3 (10, 1, -10);

		enemy1 = GameManager.CreateCharacterAt("Enemy1", ClassType.MERCENARY, TeamColor.BLUE_TEAM, false, spawn1);
		enemy2 = GameManager.CreateCharacterAt("Enemy2", ClassType.MERCENARY, TeamColor.BLUE_TEAM, false, spawn2);
		enemy3 = GameManager.CreateCharacterAt("Enemy3", ClassType.MERCENARY, TeamColor.BLUE_TEAM, false, spawn3);
		ally1 = GameManager.CreateCharacterAt("Ally1", ClassType.MERCENARY, TeamColor.RED_TEAM, false, spawn7);
		ally2 = GameManager.CreateCharacterAt("Ally2", ClassType.MERCENARY, TeamColor.RED_TEAM, false, spawn8);
		ally3 = GameManager.CreateCharacterAt("Ally3", ClassType.MERCENARY, TeamColor.RED_TEAM, false, spawn9);

		enemy1.UseAction (ActionType.PURSUIT_AND_ATTACK, ally1);
		enemy2.UseAction (ActionType.PURSUIT_AND_ATTACK, ally2);
		enemy3.UseAction (ActionType.PURSUIT_AND_ATTACK, ally3);
		ally1.UseAction (ActionType.PURSUIT_AND_ATTACK, enemy1);
		ally2.UseAction (ActionType.PURSUIT_AND_ATTACK, enemy2);
		ally3.UseAction (ActionType.PURSUIT_AND_ATTACK, enemy3);
	}

	public void SpawnHunters()
	{
		if(enemy4!=null) Destroy(enemy4.CharacterGO);
		if(enemy5!=null) Destroy(enemy5.CharacterGO);
		if(ally4!=null) Destroy(ally4.CharacterGO);
		if(ally5!=null) Destroy(ally5.CharacterGO);

		Vector3 spawn4 = new Vector3 (-13, 1, -3);
		Vector3 spawn5 = new Vector3 (-13, 1, -8);
		Vector3 spawn10 = new Vector3 (13, 1, -3);
		Vector3 spawn11 = new Vector3 (13, 1, -8);

		enemy4 = GameManager.CreateCharacterAt("Enemy4", ClassType.HUNTER, TeamColor.BLUE_TEAM, false, spawn4);
		enemy5 = GameManager.CreateCharacterAt("Enemy5", ClassType.HUNTER, TeamColor.BLUE_TEAM, false, spawn5);
		ally4 = GameManager.CreateCharacterAt("Ally4", ClassType.HUNTER, TeamColor.RED_TEAM, false, spawn10);
		ally5 = GameManager.CreateCharacterAt("Ally5", ClassType.HUNTER, TeamColor.RED_TEAM, false, spawn11);

		enemy4.UseAction (ActionType.PURSUIT_AND_ATTACK, ally1);
		enemy5.UseAction (ActionType.PURSUIT_AND_ATTACK, ally3);
		ally4.UseAction (ActionType.PURSUIT_AND_ATTACK, enemy1);
		ally5.UseAction (ActionType.PURSUIT_AND_ATTACK, enemy3);
	}

	public void SpawnDiciples()
	{
		if(enemy6!=null) Destroy(enemy6.CharacterGO);		
		if(ally6!=null) Destroy(ally6.CharacterGO);
		
		Vector3 spawn6 = new Vector3 (-15, 1, -5);		
		Vector3 spawn12 = new Vector3 (15, 1, -5);
		
		
		enemy6 = GameManager.CreateCharacterAt("Enemy6", ClassType.DISCIPLE, TeamColor.BLUE_TEAM, false, spawn6);		
		ally6 = GameManager.CreateCharacterAt("Ally6", ClassType.DISCIPLE, TeamColor.RED_TEAM, false, spawn12);
		
		enemy6.UseAction (ActionType.PURSUIT_AND_ATTACK, ally2);		
		ally6.UseAction (ActionType.PURSUIT_AND_ATTACK, enemy2);
	}

	public void RefreshTargets()
	{
		enemy1.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		enemy2.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		enemy3.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		enemy4.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		enemy5.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		enemy6.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);	

		ally1.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		ally2.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		ally3.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		ally4.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		ally5.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
		ally6.UseAction (ActionType.PURSUIT_AND_ATTACK, Vector3.zero);

		//enemy1.SetTarget (Vector3.up);
		//enemy2.SetTarget (Vector3.up);
		//enemy3.SetTarget (Vector3.up);
		//enemy4.SetTarget (Vector3.up);
		//enemy5.SetTarget (Vector3.up);
		//enemy6.SetTarget (Vector3.up);		

		//ally1.SetTarget (Vector3.up);
		//ally2.SetTarget (Vector3.up);
		//ally3.SetTarget (Vector3.up);	
		//ally4.SetTarget (Vector3.up);
		//ally5.SetTarget (Vector3.up);		
		//ally6.SetTarget (Vector3.up);
	}
	 
	public void SpanwMage()
	{
		SpanwPlayer (ClassType.MAGE);
	}

	public void SpanwArcher()
	{
		SpanwPlayer (ClassType.ARCHER);
	}

	public void SpanwWarrior()
	{
		SpanwPlayer (ClassType.WARRIOR);
	}

	public void SpanwPlayer(ClassType playerClass)
	{
		if (player != null)
			Destroy (player.CharacterGO);

		player = GameManager.CreateCharacterAt ("Player", playerClass, TeamColor.RED_TEAM, true, new Vector3(10, 1, 10));
	}

	public void DebugCode()
	{
		if(enemy2!=null) Destroy(enemy2.CharacterGO);
		if(enemy6!=null) Destroy(enemy6.CharacterGO);
		if(ally2!=null) Destroy(ally2.CharacterGO);
		if(ally6!=null) Destroy(ally6.CharacterGO);

		Vector3 spawn2 = new Vector3 (-10, 1, -5);
		Vector3 spawn6 = new Vector3 (-15, 1, -5);		
		Vector3 spawn8 = new Vector3 (10, 1, -5);
		Vector3 spawn12 = new Vector3 (15, 1, -5);

		enemy2 = GameManager.CreateCharacterAt("Enemy2", ClassType.MERCENARY, TeamColor.BLUE_TEAM, false, spawn2);
		enemy6 = GameManager.CreateCharacterAt("Enemy6", ClassType.DISCIPLE, TeamColor.BLUE_TEAM, false, spawn6);		
		ally2 = GameManager.CreateCharacterAt("Ally2", ClassType.MERCENARY, TeamColor.RED_TEAM, false, spawn8);
		ally6 = GameManager.CreateCharacterAt("Ally6", ClassType.DISCIPLE, TeamColor.RED_TEAM, false, spawn12);
		
		enemy6.UseAction (ActionType.PURSUIT_AND_ATTACK, ally2);
		ally6.UseAction (ActionType.PURSUIT_AND_ATTACK, enemy2);	
	}
}
