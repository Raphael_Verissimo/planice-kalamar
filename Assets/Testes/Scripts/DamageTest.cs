﻿using UnityEngine;
using System.Collections;

public class DamageTest : MonoBehaviour 
{
	private CharacterController character; 
	private LifeBar lifeBar; 	
	private bool lifeBarUpdated; 

	private GameObject damageTestCanvas;
	//private GameObject currentLifeTestCanvas;
	private GameObject maxLifeTestCanvas;

	private UnityEngine.UI.InputField damageAmount;
	//private UnityEngine.UI.InputField currentLife;
	private UnityEngine.UI.InputField maxLife;

	// Use this for initialization
	void Start () 
	{
		character = GameManager.CreateCharacter ("Player", ClassType.ARCHER, TeamColor.RED_TEAM, true);

		lifeBar = GameObject.Find ("LifeBar").GetComponent<LifeBar> ();

		damageTestCanvas = GameObject.Find("DamageInput");
		//currentLifeTestCanvas = GameObject.Find("CurrentLife");
		maxLifeTestCanvas = GameObject.Find("MaxLife");

		damageAmount = damageTestCanvas.GetComponent <UnityEngine.UI.InputField> ();
		//currentLife = currentLifeTestCanvas.GetComponent <UnityEngine.UI.InputField> ();
		maxLife = maxLifeTestCanvas.GetComponent <UnityEngine.UI.InputField> ();

		damageAmount.text = "150";
		//currentLife.text = "1000";
		maxLife.text = "1000";
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!lifeBarUpdated)
		{
			lifeBarUpdated = true;
			//currentLife.text = lifeBar.CurrentLife.ToString();
			maxLife.text = lifeBar.MaxLife.ToString();
		}
	}

	public void ApplyDamage()
	{
		string damageValue = damageAmount.text;
		int damage = int.Parse (damageValue);

		character.ApplyEffect (ActionEffectTypes.PHYSICAL_DAMAGE, character, damage, 0);

		lifeBarUpdated = false;
	}

	public void ApplyHealing()
	{
		string damageValue = damageAmount.text;
		int damage = int.Parse (damageValue);

		character.ApplyEffect (ActionEffectTypes.HEALING_LIFE, character, damage, 0);

		lifeBarUpdated = false;
	}

	public void SetUpBar()
	{
		character.CharacterModel.MaxHitPoints = int.Parse (maxLife.text);

		GameManager.SetUpLifeBar (character);
	}
}
