﻿using UnityEngine;
using System.Collections;

public class SkillTest : MonoBehaviour 
{
	public CharacterController enemy1;
	public CharacterController enemy2;
	public CharacterController enemy3;
	public CharacterController enemy4;
	public CharacterController enemy5;
	public CharacterController enemy6;
	public CharacterController enemy7;
	public CharacterController enemy8;
	public CharacterController enemy9;
	public CharacterController enemy10;
	public CharacterController enemy11;
	public CharacterController enemy12;
	public CharacterController enemy13;
	public CharacterController enemy14;

	public CharacterController player;

	// Use this for initialization
	void Start ()
	{
		//ReSpawn ();

		//SpanwPlayer ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(enemy11)
		{
			if(enemy11.CharacterGO.transform.position.x >= -11)
				enemy11.MoveTo (new Vector3 (-25, 0, 20), false);
			
			if(enemy11.CharacterGO.transform.position.x <= -24)
				enemy11.MoveTo (new Vector3 (-10, 0, 20), false);

			if(enemy11.IsLocked)
				enemy11.MoveTo (new Vector3 (-10, 0, 20), false);
		}

		if(enemy12)
		{
			enemy12.UseAction(ActionType.FIREBALL_ACTION, Vector3.zero);
			enemy12.SetTarget(new Vector3 (45, 0, -45));
		}

		if(enemy13 && enemy14)
		{
			if(player)
			{
				enemy13.UseAction(ActionType.PURSUIT_AND_ATTACK, player);
				enemy14.UseAction(ActionType.PURSUIT_AND_ATTACK, player);
			}
		}
	}

	public void ReSpawn()
	{
		if(enemy1!=null) Destroy(enemy1.CharacterGO);
		if(enemy2!=null) Destroy(enemy2.CharacterGO);
		if(enemy3!=null) Destroy(enemy3.CharacterGO);
		if(enemy4!=null) Destroy(enemy4.CharacterGO);
		if(enemy5!=null) Destroy(enemy5.CharacterGO);
		if(enemy6!=null) Destroy(enemy6.CharacterGO);
		if(enemy7!=null) Destroy(enemy7.CharacterGO);
		if(enemy8!=null) Destroy(enemy8.CharacterGO);
		if(enemy9!=null) Destroy(enemy9.CharacterGO);
		if(enemy10!=null) Destroy(enemy10.CharacterGO);
		if(enemy11!=null) Destroy(enemy11.CharacterGO);
		if(enemy12!=null) Destroy(enemy12.CharacterGO);
		if(enemy13!=null) Destroy(enemy13.CharacterGO);
		if(enemy14!=null) Destroy(enemy14.CharacterGO);

		Vector3 spawn1 = new Vector3 (-10, 1, -10);
		Vector3 spawn2 = new Vector3 (-13, 1, -10);
		Vector3 spawn3 = new Vector3 (-16, 1, -10);
		Vector3 spawn4 = new Vector3 (-19, 1, -10);
		Vector3 spawn5 = new Vector3 (-22, 1, -10);
		Vector3 spawn6 = new Vector3 (-25, 1, -10);
		Vector3 spawn7 = new Vector3 (10, 1, 10);
		Vector3 spawn8 = new Vector3 (10, 1, 15);
		Vector3 spawn9 = new Vector3 (15, 1, 10);
		Vector3 spawn10 = new Vector3 (15, 1, 15);
		Vector3 spawn11 = new Vector3 (-10, 1, 20);
		Vector3 spawn12 = new Vector3 (-20, 1, 15);
		Vector3 spawn13 = new Vector3 (-23, 1, 10);
		Vector3 spawn14 = new Vector3 (-17, 1, 15);
		
		enemy1 = GameManager.CreateCharacterAt("Enemy1", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false, spawn1);
		enemy2 = GameManager.CreateCharacterAt("Enemy2", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false, spawn2);
		enemy3 = GameManager.CreateCharacterAt("Enemy3", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false, spawn3);
		enemy4 = GameManager.CreateCharacterAt("Enemy4", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false, spawn4);
		enemy5 = GameManager.CreateCharacterAt("Enemy5", ClassType.WARRIOR, TeamColor.RED_TEAM, false, spawn5);
		enemy6 = GameManager.CreateCharacterAt("Enemy6", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false, spawn6);
		enemy7 = GameManager.CreateCharacterAt("Enemy7", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false, spawn7);
		enemy8 = GameManager.CreateCharacterAt("Enemy8", ClassType.WARRIOR, TeamColor.RED_TEAM, false, spawn8);
		enemy9 = GameManager.CreateCharacterAt("Enemy9", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false, spawn9);
		enemy10 = GameManager.CreateCharacterAt("Enemy10", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false, spawn10);
		enemy11 = GameManager.CreateCharacterAt("Enemy11", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false, spawn11);
		enemy12 = GameManager.CreateCharacterAt("Enemy12", ClassType.MAGE, TeamColor.BLUE_TEAM, false, spawn12);
		enemy13 = GameManager.CreateCharacterAt("Enemy13", ClassType.HUNTER, TeamColor.BLUE_TEAM, false, spawn13);
		enemy14 = GameManager.CreateCharacterAt("Enemy14", ClassType.DISCIPLE, TeamColor.BLUE_TEAM, false, spawn14);

		enemy11.MoveTo (new Vector3 (-25, 0, 20), false);
	}
	 
	public void SpanwMage()
	{
		SpanwPlayer (ClassType.MAGE);
	}

	public void SpanwArcher()
	{
		SpanwPlayer (ClassType.ARCHER);
	}

	public void SpanwWarrior()
	{
		SpanwPlayer (ClassType.WARRIOR);
	}

	public void SpanwPlayer(ClassType playerClass)
	{
		if (player != null)
			Destroy (player.CharacterGO);

		player = GameManager.CreateCharacter ("Player", playerClass, TeamColor.RED_TEAM, true);
	}
}
