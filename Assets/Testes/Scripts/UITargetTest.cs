﻿using UnityEngine;
using System.Collections;

public class UITargetTest : MonoBehaviour 
{
	private GameObject characterGO; 

	private GameObject skillShotCanvas;
	private UnityEngine.UI.InputField skillShotRange;

	private GameObject areaCanvas;
	private GameObject areaRangeCanvas;
	private UnityEngine.UI.InputField areaTargetArea;
	private UnityEngine.UI.InputField areaTargetRange;

	private bool isDrawingSkillShot;
	private bool isDrawingAreaTarget;	
	private bool isDrawingAttackPlayer;

	// Use this for initialization
	void Start () 
	{
		characterGO = GameObject.Find("Character");

		skillShotCanvas = GameObject.Find("SkillShotInput");
		
		areaCanvas = GameObject.Find("AreaInput");
		areaRangeCanvas = GameObject.Find("AreaRangeInput");

		skillShotRange = skillShotCanvas.GetComponent <UnityEngine.UI.InputField> ();
		areaTargetArea = areaCanvas.GetComponent <UnityEngine.UI.InputField> ();
		areaTargetRange = areaRangeCanvas.GetComponent <UnityEngine.UI.InputField> ();

		skillShotRange.text = "20";

		areaTargetArea.text = "10";
		areaTargetRange.text = "10";
	}

	// Update is called once per frame
	void Update () 
	{
		if (isDrawingSkillShot)
			GameManager.DrawSkillShotTarget(characterGO.transform.position, float.Parse(skillShotRange.text));
	
		if (isDrawingAreaTarget)
		{
			GameManager.DrawRangedAreaTarget(characterGO.transform.position, float.Parse(areaTargetRange.text));
			GameManager.DrawAreaTargetInRange(characterGO.transform.position, float.Parse(areaTargetArea.text), float.Parse(areaTargetRange.text));
		}

		if (isDrawingAttackPlayer)
			GameManager.DrawAttackPlayer ();
	}

	public void ClearAllTargets()
	{
		isDrawingSkillShot = false;
		isDrawingAreaTarget = false;
		isDrawingAttackPlayer = false;
		GameManager.ClearTargets ();
	}

	public void UseSkillShot()
	{
		isDrawingSkillShot = true;
	}

	public void UseAreaTarget()
	{
		isDrawingAreaTarget = true;
	}

	public void UseAttackTarget()
	{
		isDrawingAttackPlayer = true;
	}
}
