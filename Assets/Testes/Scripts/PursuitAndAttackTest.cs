﻿using UnityEngine;
using System.Collections;

public class PursuitAndAttackTest : MonoBehaviour 
{
	public CharacterController enemy1;
	public CharacterController enemy2;
	public CharacterController enemy3;
	public CharacterController enemy4;

	public CharacterController player;

	// Use this for initialization
	void Start ()
	{
		enemy1 = GameManager.CreateCharacter ("Enemy1", ClassType.MAGE, TeamColor.BLUE_TEAM, false);
		enemy2 = GameManager.CreateCharacter ("Enemy2", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false);
		enemy3 = GameManager.CreateCharacter ("Enemy3", ClassType.ARCHER, TeamColor.BLUE_TEAM, false);
		enemy4 = GameManager.CreateCharacter ("Enemy4", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false);

		player = GameManager.CreateCharacter ("Player", ClassType.ARCHER, TeamColor.RED_TEAM, true);

		enemy1.MoveTo (new Vector3 (25, 0, 25), false);
		enemy2.MoveTo (new Vector3 (25, 0, -25), false);
		enemy3.MoveTo (new Vector3 (-25, 0, 25), false);
		enemy4.MoveTo (new Vector3 (-5, 0, -5), false);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(enemy1.CharacterGO.transform.position.x >= 23)
			enemy1.MoveTo (new Vector3 (-25, 0, -25), false);
		
		if(enemy1.CharacterGO.transform.position.x <= -23)
			enemy1.MoveTo (new Vector3 (25, 0, 25), false);
		if(enemy2.CharacterGO.transform.position.x >= 23)
			enemy2.MoveTo (new Vector3 (-25, 0, 25), false);
		
		if(enemy2.CharacterGO.transform.position.x <= -23)
			enemy2.MoveTo (new Vector3 (25, 0, -25), false);

		if(enemy3.CharacterGO.transform.position.x >= 23)
			enemy3.MoveTo (new Vector3 (-25, 0, 25), false);		
		
		if(enemy3.CharacterGO.transform.position.x <= -23)
			enemy3.MoveTo (new Vector3 (25, 0, -25), false);

		if(enemy4.CharacterGO.transform.position.x >= 23)
			enemy4.MoveTo (new Vector3 (-25, 0, -25), false);		
		
		if(enemy4.CharacterGO.transform.position.x <= -23)
			enemy4.MoveTo (new Vector3 (25, 0, 25), false);	

		/*
		if(!enemy1.IsAlive)
		{
			enemy1 = GameManager.CreateCharacter ("Enemy1", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false);
			enemy1.MoveTo (new Vector3 (25, 0, 25), false);
		}

		if(!enemy2.IsAlive)
		{
			enemy2 = GameManager.CreateCharacter ("Enemy2", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false);
			enemy2.MoveTo (new Vector3 (25, 0, -25), false);
		}

		if(!enemy3.IsAlive)
		{
			enemy3 = GameManager.CreateCharacter ("Enemy3", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false);
			enemy3.MoveTo (new Vector3 (-25, 0, 25), false);
		}

		if(!enemy4.IsAlive)
		{
			enemy4 = GameManager.CreateCharacter ("Enemy4", ClassType.WARRIOR, TeamColor.BLUE_TEAM, false);
			enemy4.MoveTo (new Vector3 (-25, 0, -25), false);
		}
		*/
	}
}
